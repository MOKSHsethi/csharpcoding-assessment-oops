﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    internal class OnPayroll : Developer
    {
        string Department;
        string Manager;
        double NetSalary;
        byte Experience;
        double TotalSalary;

        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Department:");
            Department = Console.ReadLine();
            Console.WriteLine("Enter the Manager Name");
            Manager = Console.ReadLine();
            Console.WriteLine("Enter the Salary");
            NetSalary = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter the experience");
            Experience = byte.Parse(Console.ReadLine());
            Calculatepayment(NetSalary, Experience);
            DisplayDetails();
        }
        public void Calculatepayment(double netSalary, byte Experience)
        {
            if (Experience > 10)
            {
                TotalSalary = (((10 / 100) * NetSalary) + ((8.5 / 100) * NetSalary) - 6500 + NetSalary);
            }
            else if (Experience > 7 && Experience < 10)
            {
                TotalSalary = (((7 / 100) * NetSalary) + ((6.5 / 100) * NetSalary) - 4100 + NetSalary);
            }
            else if (Experience > 5 && Experience < 7)
            {
                TotalSalary = (((4.1 / 100) * NetSalary) + ((3.8 / 100) * NetSalary) - 1800 + NetSalary);
            }
            else
            {
                TotalSalary = (((1.9 / 100) * NetSalary) + ((2.0 / 100) * NetSalary) - 1200 + NetSalary);
            }

        }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("The Department is {0}", Department);
            Console.WriteLine("The Manager is {0}", Manager);
            Console.WriteLine("The Basic Salary is {0}", NetSalary);
            Console.WriteLine("The Total salary is {0}", TotalSalary);
        }


    }
}

