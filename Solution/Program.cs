﻿
namespace Solution;




    internal class Program
    {
    static void Main(string[] args)
    {
        try
        {
            while (true)
            {
                Console.WriteLine("Create Developer Menu\nEnter  Your Choice\n1.OnPayroll\n2.Oncontract basis");
                int ch = Byte.Parse(Console.ReadLine());
                List<Developer> Dev = new List<Developer>();
                Developer developer = new Developer();
                for (int i = 0; i <2; i++)
                {
                    switch (ch)
                    {
                        case 1:
                            {
                                developer = new OnPayroll(); break;
                            }
                        case 2:
                            {
                                developer = new OnContract(); break;
                            }
                        default:
                            Console.WriteLine("Enter A Valid input");
                            break;

                    }
                    developer.GetDetails();
                    Dev.Add(developer);
                }
                
                Console.WriteLine("Displaying All the records");
                var list1 = (from x in Dev select x).ToList();
                foreach (Developer d in list1)
                    d.DisplayDetails();
                
                Console.WriteLine("Displaying Records in which Name contains letter D");
                var list2 = (from x in Dev where x.Name.Contains("D") select x).ToList();
                foreach (Developer d in list2)
                    d.DisplayDetails();


                //Console.WriteLine("Displaying Records in which Salary is more than 20000");
                //var list3 = (from x in Dev where x.NetSalary > 20000 select x).ToList();

                //foreach (Developer dev in list3)
                //    dev.DisplayDetails();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine("Enter The valid Options");
        }

    }
    }




