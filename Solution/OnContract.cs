﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    internal class OnContract : Developer
    {

        int Duration;
        double PerDayCharge;
        double Payment_Amount;
        public OnContract() : base() { }





        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Duration of Contract:");
            Duration = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Charges per Day:");
            PerDayCharge = Convert.ToDouble(Console.ReadLine());
            CalculatePayment();
        }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("charges are " + PerDayCharge);
            Console.WriteLine("duration is " + Duration);
            Console.WriteLine("Total payment done " + Payment_Amount);
        }
        public void CalculatePayment()
        {
            Payment_Amount = PerDayCharge * Duration;
            DisplayDetails();
        }


    }
}
