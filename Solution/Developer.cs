﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Solution
{
    internal class Developer
    {
        int ID;
        public string Name;
        DateTime DateOfjoining;
        string ProjectAssigned;


        public virtual void GetDetails()
        {
            Console.WriteLine("Enter Id:");
            ID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Developer's Name:");
            Name = Console.ReadLine();
            Console.WriteLine("Enter Joining Date:");
            DateOfjoining = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter the Project Assigned:");
            ProjectAssigned = Console.ReadLine();
        }
        public void DisplayDetails()
        {
            Console.WriteLine("The Id is {0}", ID);
            Console.WriteLine("The Name of the developer is {0}", Name);
            Console.WriteLine("The Joining date of Developer is {0}", DateOfjoining);
            Console.WriteLine("The Project Assigned to the developer is {0}", ProjectAssigned);
        }





    }

}
